# make 1G txt data file
```bash
dd if=/dev/zero of=data.txt bs=1M count=1024
```
# compress with tools
```bash
gzip -c data.txt > data.txt.gz
tar -cf data.txt.tar data.txt
xz -zk data.txt
7za a -t7z data.txt.7z data.txt
```
# compress with bzip2
```bash
bzip2 -zk data.txt
```
# make unlimited size bzip2 archive (3GB in this case)
```bash
dd if=/dev/zero bs=1G count=3 | bzip2 -c > fun.bz2
```
# start extracting that file
```bash
bzip2 -d fun.bz2
```
#########################################
# nu11secur1ty - test

# Make 3G file for test
```bash
dd if=/dev/zero bs=1MB count=3000 | bzip2 -c > fun.txt.bz2
```
# start extracting that file
```bash
bzip2 -d fun.txt.bz2
```
------------------------------------------

# Manual
# make 4MB txt data file
```bash
dd if=/dev/zero of=fun.txt bs=4MB count=1
```
# make bzip2 archive
```bash
bzip2 fun.txt
```
# chech the size after compress
```bash
du -h fun.txt
```
# start extracting that file
```bash
bzip2 -d fun.txt.bz2
```
